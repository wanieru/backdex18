﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    abstract class AbstractAction : ISavable
    {
        float ISavable.GetSaveOrder(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.Load(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.Save(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.SaveReset(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        public abstract void ExecuteAction();
        public static AbstractAction LoadAction(SaveManager saveManager)
        {
            AbstractAction _return = null;
            int identifier = saveManager.ReadInt();
            switch(identifier)
            {
                case 0:
                    _return = new BackupAction();
                    break;
                case 1:
                    _return = new FtpDownloadAction();
                    break;
                case 2:
                    _return = new IndexAction();
                    break;
                case 3:
                    _return = new WebRequestAction();
                    break;
            }
            ((ISavable)_return)?.Load(saveManager);
            return _return;
        }
        protected int GetActionIdentifier()
        {
            if (this is BackupAction)
                return 0;
            if (this is FtpDownloadAction)
                return 1;
            if (this is IndexAction)
                return 2;
            if (this is WebRequestAction)
                return 3;
            return -1;
        }
    }
}
