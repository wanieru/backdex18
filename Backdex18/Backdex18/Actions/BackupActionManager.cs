﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    class BackupActionManager : ISavable
    {
        public static BackupActionManager I;
        private SaveManager saveManager;
        private List<AbstractAction> backupActions;
        private BackupActionManager()
        {
            this.backupActions = new List<AbstractAction>();
        }
        public static BackupActionManager LoadFromKey(string key)
        {
            BackupActionManager manager = new BackupActionManager();
            manager.saveManager = new SaveManager(key, 0, manager);
            if(!manager.saveManager.Load())
            {
                return null;
            }
            return manager;
        }
        public void ExecuteActions()
        {

        }
        float ISavable.GetSaveOrder(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.Load(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.Save(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }

        void ISavable.SaveReset(SaveManager saveManager)
        {
            throw new NotImplementedException();
        }
    }
}
