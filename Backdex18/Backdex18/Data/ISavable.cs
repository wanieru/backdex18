﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{

    public interface ISavable
    {
        /// <summary>
        /// Write this object's save data using the SaveManager
        /// </summary>
        void Save(SaveManager saveManager);

        /// <summary>
        /// Load data from the SaveManager in the same order as it's saved.
        /// </summary>
        void Load(SaveManager saveManager);
        /// <summary>
        /// Resets the gameobject before loading new data in.
        /// </summary>
        void SaveReset(SaveManager saveManager);
        /// <summary>
        /// Return a float used to sort savable elements in the game. Useful to keep consistent save order when elements might not load in the same order.
        /// </summary>
        /// <returns></returns>
        float GetSaveOrder(SaveManager saveManager);
    }
}
