﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Backdex18
{
    /// <summary>
    /// Responsible for saving and loading the game, and writing and reading data from a save stream.
    /// </summary>
    public class SaveManager
    {
        private string saveFile;
        private int saveVersion;

        /// <summary>
        /// List of objects to call save and load on.
        /// </summary>
        private List<ISavable> savables;

        /// <summary>
        /// The loaded/currently writing data
        /// </summary>
        private List<byte> saveBuffer;
        private int bufferIndex;
        private bool isCurrentlyActive;
        private bool isLoading;
        private Scrambler scrambler;
        public SaveManager(string encryptionKey, int saveVersion = 0, params ISavable[] savables)
        {
            this.scrambler = new Scrambler(encryptionKey);
            this.saveFile = Scrambler.CalculateHash(encryptionKey) +".profile";
            this.saveVersion = saveVersion;
            saveBuffer = new List<byte>();
            bufferIndex = 0;
            isCurrentlyActive = false;
            this.savables = savables.ToList();
            OrderSavables();
        }
        /// <summary>
        /// If currently active, does nothing. Otherwise, deletes the save file
        /// </summary>
        public void DeleteSaveFile()
        {
            if (isCurrentlyActive)
                return;
            if (!File.Exists(saveFile))
                return;
            File.Delete(saveFile);
        }

        /// <summary>
        /// Resets the buffer
        /// </summary>
        private void ResetBuffer()
        {
            saveBuffer.Clear();
            bufferIndex = 0;
        }
        /// <summary>
        /// If currently active, does nothing. Otherwise, adds the specified Isavable element to the list of savables called when saving and loading the game
        /// </summary>
        public void AddSavable(ISavable savable)
        {
            if (isCurrentlyActive)
                return;
            if (savables.Contains(savable))
                return;
            savables.Add(savable);
            OrderSavables();
        }
        private void OrderSavables()
        {
            savables = savables.OrderBy(saveable => saveable.GetSaveOrder(this)).ToList();
        }
        /// <summary>
        /// Write all game data and writes it to a file
        /// </summary>
        public void Save()
        {
            if (isCurrentlyActive)
                return;
            try
            {
                isCurrentlyActive = true;
                isLoading = false;
                ResetBuffer();
                WriteInt(saveVersion);
                foreach (ISavable save in savables)
                {
                    save.Save(this);
                }
                byte[] scrambled;
                scrambler.Scramble(true, saveBuffer.ToArray(), out scrambled);
                File.WriteAllBytes(saveFile, scrambled);
            }
            finally
            {
                isCurrentlyActive = false;
            }
        }
        /// <summary>
        /// If currently active, does nothing. Otherwise, reads the save data and checks the version. If mismatched, does nothing. Otherwise, calls reset on all ISavables then calls load on all ISavables.
        /// </summary>
        public bool Load()
        {
            if (isCurrentlyActive)
                return false;
            try
            {
                isCurrentlyActive = true;
                isLoading = true;
                if (!File.Exists(saveFile))
                {
                    return true;
                }
                ResetBuffer();
                byte[] encrypted = File.ReadAllBytes(saveFile);
                byte[] decrypted;
                scrambler.Scramble(false, encrypted, out decrypted);
                if (decrypted == null)
                    return false;
                saveBuffer = decrypted.ToList();
                int version = ReadInt();
                if (version != saveVersion)
                    return false;
                foreach (ISavable save in savables)
                {
                    save.SaveReset(this);
                }
                foreach (ISavable save in savables)
                {
                    save.Load(this);
                }
            }
            finally
            {
                isCurrentlyActive = false;
            }
            return true;
        }
        /// <summary>
        /// If currently saving, does nothing. Otherwise, reads 4 bytes from the buffer and returns it as an int
        /// </summary>
        public int ReadInt()
        {
            if (!isLoading)
                return 0;
            int returnValue = BitConverter.ToInt32(saveBuffer.ToArray(), bufferIndex);
            bufferIndex += 4;
            return returnValue;
        }
        /// <summary>
        /// If currently saving, does nothing. Otherwise, reads 4 bytes as a float
        /// </summary>
        public float ReadFloat()
        {
            if (!isLoading)
                return 0f;
            float returnValue = BitConverter.ToSingle(saveBuffer.ToArray(), bufferIndex);
            bufferIndex += 4;
            return returnValue;
        }
        /// <summary>
        /// If currently saving, does nothing. Otherwise, reads 4 bytes as an int, and returns that many bytes as a utf8 encoded string.
        /// </summary>
        public string ReadString()
        {
            if (!isLoading)
                return "";
            int length = ReadInt();
            string returnString = Encoding.UTF8.GetString(saveBuffer.ToArray(), bufferIndex, length);
            bufferIndex += length;
            return returnString;
        }
        /// <summary>
        /// If currently saving, does nothing. Otherwise, reads one byte and returns true if it's greater than 0.
        /// </summary>
        public bool ReadBool()
        {
            if (!isLoading)
                return false;
            bool returnBool = saveBuffer[bufferIndex] > 0;
            bufferIndex++;
            return returnBool;
        }
        /// <summary>
        /// If currently saving, does nothing. Otherwise, reads one byte and returns it.
        /// </summary>
        public byte ReadByte()
        {
            if (!isLoading)
                return 0;
            Byte returnByte = saveBuffer[bufferIndex];
            bufferIndex++;
            return returnByte;
        }
        /// <summary>
        /// If currently loading, does nothing. Otherwise, writes one byte to the buffer.
        /// </summary>
        public void WriteByte(Byte b)
        {
            if (isLoading)
                return;
            saveBuffer.Add(b);
            bufferIndex++;
        }
        /// <summary>
        /// If currently loading, does nothing. Otherwise, writes one byte (1) if true, 0 otherwise.
        /// </summary>
        public void WriteBool(bool b)
        {
            if (isLoading)
                return;
            saveBuffer.Add((byte)(b ? 1 : 0));
            bufferIndex++;
        }
        /// <summary>
        /// If currently loading, does nothing. Otherwise, encodes the string in utf8, writes the length as a 4-byte int32 and then the encoded string. 
        /// </summary>
        /// <param name="str"></param>
        public void WriteString(string str)
        {
            if (isLoading)
                return;
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            WriteInt(bytes.Length);
            saveBuffer.AddRange(bytes);
            bufferIndex += bytes.Length;
        }
        /// <summary>
        /// If currently loading, does nothing. Otherwise, writes a float as 4 bytes.
        /// </summary>
        public void WriteFloat(float f)
        {
            if (isLoading)
                return;
            saveBuffer.AddRange(BitConverter.GetBytes(f));
            bufferIndex += 4;
        }
        /// <summary>
        /// If currently loading, does nothing. Otherwise, writes an int as 4 bytes.
        /// </summary>
        public void WriteInt(int i)
        {
            if (isLoading)
                return;
            saveBuffer.AddRange(BitConverter.GetBytes(i));
            bufferIndex += 4;
        }
    }
}