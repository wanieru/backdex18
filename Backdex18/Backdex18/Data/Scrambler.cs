﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using ZetaLongPaths;

namespace Backdex18
{
    class Scrambler
    {
        private const string KEY_SALT = "c248700abbadf899646c67966eaaaf1a5de03ed2602f6fff3bd1b596721da82673c7ac0a45732d1b3ac6570520454fc4fa96e7df8947e62c04403bec41b39c6e"; //Sha512 of "qzformsadmin"
        private const string ID_SALT = "9690d2711f9047bd045e4186e2bca1e3d3c17464bd75574ef488a2c160eaf7caa4241af4b757b8654f9b680a951595e8dee762e0efc14a3bcb9eea96a6abb9aa"; //Sha512 of "qzformsadminid"
        private const int KEY_ROUNDS = 1024; //2^10
        private const int IV_LENGTH = 16;
        private readonly byte[] key; //Key to use for encryption
        private readonly byte[] id; //Id hash to validate if the file is encrypted with the loaded key
        private readonly Random random;
        private readonly RijndaelManaged rijndael;
        public Scrambler(string argKey)
        {
            random = new Random(Environment.TickCount);
            key = CalculateHash(argKey, KEY_SALT, 32);
            id = CalculateHash(argKey, ID_SALT, KEY_ROUNDS);
            rijndael = new RijndaelManaged
            {
                Padding = PaddingMode.ISO10126,
                Mode = CipherMode.CBC,
                BlockSize = 16 * 8,
                Key = key
            };
        }
        private static byte[] GetBytes(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
        private static string GetString(byte[] bytes)
        {
            return Encoding.UTF8.GetString(bytes);
        }
        private static byte[] CalculateHash(string argKey, string hash, int returnLength, int keyRounds = KEY_ROUNDS)
        {
            return CalculateHash(GetBytes(argKey + hash), returnLength, keyRounds);
        }
        private static byte[] CalculateHash(byte[] argKey, int returnLength, int keyRounds = KEY_ROUNDS)
        {
            var sha = new SHA512Cng();
            for (var i = 0; i < keyRounds; i++)
            {
                argKey = sha.ComputeHash(argKey);
            }
            return argKey.Take(returnLength).ToArray();
        }
        public static string CalculateHash(string argKey)
        {
            return BitConverter.ToString(CalculateHash(argKey, "", int.MaxValue, 1)).ToLower().Replace("-", "");
        }
        private void Scramble(Stream sourceStream, Stream targetStream, byte[] salt, bool encrypt)
        {
            var binaryReader = new BinaryReader(sourceStream);
            rijndael.IV = salt;
            var crypto = encrypt ? rijndael.CreateEncryptor(rijndael.Key, rijndael.IV) : rijndael.CreateDecryptor(rijndael.Key, rijndael.IV);
            var cryptostream = new CryptoStream(targetStream, crypto, CryptoStreamMode.Write);
            while (sourceStream.Position < sourceStream.Length)
            {
                var remaining = (sourceStream.Length - sourceStream.Position);
                var int32ReadCount = remaining > int.MaxValue ? int.MaxValue : (int)remaining;
                var readBytes = binaryReader.ReadBytes(int32ReadCount);
                cryptostream.Write(readBytes, 0, readBytes.Length);
            }
            cryptostream.FlushFinalBlock();
        }
        public bool Scramble(bool encrypt, Stream sourceStream, Stream targetStream)
        {
            var salt = new byte[IV_LENGTH];
            if (encrypt)
            {
                random.NextBytes(salt);
                targetStream.Write(salt, 0, salt.Length);
                targetStream.Write(id, 0, id.Length);
                targetStream.Flush();
            }
            else
            {
                sourceStream.Read(salt, 0, salt.Length);
                var fileId = new byte[id.Length];
                sourceStream.Read(fileId, 0, fileId.Length);
                if (!id.SequenceEqual(fileId))
                    return false;
            }
            Scramble(sourceStream, targetStream, salt, encrypt);
            return true;
        }
        public bool Scramble(bool encrypt, byte[] content, out byte[] target)
        {
            var sourceStream = new MemoryStream(content);
            var targetStream = new MemoryStream();
            var scrambleResult = Scramble(encrypt, sourceStream, targetStream);
            if (!scrambleResult)
            {
                target = null;
                return false;
            }
            target = targetStream.ToArray();
            return true;
        }
        public string ReadFileString(ZlpFileInfo filePath)
        {
            var unscrambled = ReadFileBytes(filePath);
            if (unscrambled == null)
                return null;
            return GetString(unscrambled);
        }
        public byte[] ReadFileBytes(ZlpFileInfo filePath)
        {
            var scrambledBytes = ZlpIOHelper.ReadAllBytes(filePath.FullName);
            byte[] unscrambled;
            var result = Scramble(false, scrambledBytes, out unscrambled);
            if (!result)
                return null;
            return unscrambled;
        }
        public void WriteFile(byte[] content, ZlpFileInfo filePath)
        {
            byte[] scrambled;
            Scramble(true, content, out scrambled);
            ZlpIOHelper.WriteAllBytes(filePath.FullName, scrambled);
        }
        public void WriteFile(string content, ZlpFileInfo filePath)
        {
            var unscrambledBytes = GetBytes(content);
            WriteFile(unscrambledBytes, filePath);
        }
    }
}
