﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    class MenuItem
    {
        protected string menuText;
        public virtual string MenuText { get{ return menuText; } }
        public Action OnClick { get; protected set; }
        public bool IsLabel{ get; protected set; }
        public MenuItem(string menuText, Action onClick)
        {
            this.menuText = menuText;
            this.OnClick = onClick;
            IsLabel = false;
        }
        public MenuItem(string menuText)
        {
            this.menuText = menuText;
            IsLabel = true;
        }
    }
}
