﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    static class InputManager
    {
        public static string ReadString(string prompt)
        {
            MenuHelper.Write(prompt + ": ", MenuColor.Normal);
            return MenuHelper.ReadLine();
        }
        public static float ReadFloat(string prompt)
        {
            string readData = null;
            float outputData;
            do
            {
                if(readData != null)
                {
                    MenuHelper.WriteLine("Error: " + readData + " should be a decimal number.", MenuColor.Error);
                }
                readData = ReadString(prompt);
            } while (!float.TryParse(readData, out outputData));
            return outputData;
        }
        public static int ReadInt(string prompt)
        {
            string readData = null;
            int outputData;
            do
            {
                if (readData != null)
                {
                    MenuHelper.WriteLine("Error: " + readData + " should be an integer number.", MenuColor.Error);
                }
                readData = ReadString(prompt);
            } while (!int.TryParse(readData, out outputData));
            return outputData;
        }
    }
}
