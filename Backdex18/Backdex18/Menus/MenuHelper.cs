﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    static class MenuHelper
    {
        private static Stack<MenuColor> menuColors = new Stack<MenuColor>();
        private static void UpdateColor()
        {
            if(menuColors.Count < 1)
            {
                menuColors.Push(MenuColor.Normal);
            }
            switch (menuColors.Peek())
            {
                default:
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MenuColor.Selected:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case MenuColor.UserInput:
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MenuColor.Title:
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MenuColor.Error:
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
            }
        }
        public static void PushColor(MenuColor color)
        {
            menuColors.Push(color);
            UpdateColor();
        }
        public static void PopColor()
        {
            menuColors.Pop();
            UpdateColor();
        }

        public static void TempColorAction(MenuColor color, Action tempAction)
        {
            PushColor(color);
            tempAction();
            PopColor();
        }
        public static object TempColorRead(MenuColor color, Func<object> tempFunc)
        {
            PushColor(color);
            var result = tempFunc();
            PopColor();
            return result;
        }
        public static void Clear()
        {
            TempColorAction(MenuColor.Normal, () => { Console.Write(" "); Console.Clear(); });
        }
        public static ConsoleKeyInfo ReadKey()
        {
            return Console.ReadKey(true);
        }
        public static void WriteLine(string data)
        {
            Console.Write(data);
            TempColorAction(MenuColor.Normal, () =>
            {
                Console.Write(" ");
                Console.WriteLine();
            });
        }
        public static void WriteLine(string data, MenuColor color)
        {
            TempColorAction(color, () =>
            {
                Console.Write(data);
                TempColorAction(MenuColor.Normal, () =>
                {
                    Console.Write(" ");
                    Console.WriteLine();
                });
            });
        }
        public static void Write(string data)
        {
            Console.Write(data);
        }
        public static void Write(string data, MenuColor color)
        {
            TempColorAction(color, () =>
            {
                Console.Write(data);
            });
        }
        public static string ReadLine()
        {
            return (string)TempColorRead(MenuColor.UserInput, () =>
            {
                return Console.ReadLine();
            });
        }
    }
}
