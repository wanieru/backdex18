﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    static class MenuManager
    {
        private static Menu _activeMenu;
        public static Menu ActiveMenu
        {
            get
            {
                return _activeMenu;
            }
            set
            {
                _activeMenu = value;
            }
        }
        public static void ShowActiveMenu()
        {
            _activeMenu?.Show();
        }
    }
}
