﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    class MainMenu : Menu
    {
        private MenuItem Settings;
        private MenuItem AddNew;
        private SettingsMenu settings;
        public MainMenu() : base("Backdex 18", null)
        {
            settings = new SettingsMenu(this);
            Settings = new MenuItem("Settings", () => { MenuManager.ActiveMenu = settings; });
            AddNew = new MenuItem("Add Action", () => { });
        }
        protected override void UpdateItems()
        {
            Random random = new Random();
            menuItems.Clear();
            menuItems.Add(new MenuItem("Actions"));
            menuItems.Add(new MenuItem(""));
            menuItems.Add(AddNew);
            menuItems.Add(Settings);
            base.UpdateItems();
        }
    }
}
