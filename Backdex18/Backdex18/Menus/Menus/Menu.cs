﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    class Menu
    {
        protected List<MenuItem> menuItems;
        protected int selectedMenuItem;
        public string Title { get; private set; }
        private MenuItem backMenuItem;
        public Menu(string title, Menu back, params MenuItem[] menuItems)
        {
            this.Title = title;
            this.menuItems = new List<MenuItem>();
            if(back != null)
            {
                backMenuItem = new MenuItem("Back to " + back.Title, () => { MenuManager.ActiveMenu = back; });
            }
            this.menuItems.AddRange(menuItems);
        }
        public void Show()
        {
            UpdateItems();
            MenuHelper.Clear();
            MenuHelper.WriteLine("===" + Title + "===", MenuColor.Title);
            for(var i=0;i<menuItems.Count;i++)
            {
                MenuHelper.PushColor(selectedMenuItem == i ? MenuColor.Selected : MenuColor.Normal);
                if (menuItems[i].IsLabel)
                {
                    MenuHelper.WriteLine(menuItems[i].MenuText);
                }
                else
                {
                    MenuHelper.WriteLine("[" + menuItems[i].MenuText + "]");
                }
                MenuHelper.PopColor();
            }
            DoInput();
        }
        protected virtual void UpdateItems()
        {
            if(backMenuItem != null && !menuItems.Contains(backMenuItem))
            {
                menuItems.Insert(0, backMenuItem);
            }
        }
        private void DoInput()
        {
            var key = MenuHelper.ReadKey();
            switch(key.Key)
            {
                case ConsoleKey.DownArrow:
                    do
                    {
                        selectedMenuItem = (selectedMenuItem + 1) % menuItems.Count;
                    } while (menuItems[selectedMenuItem].IsLabel);
                    break;
                case ConsoleKey.UpArrow:
                    do
                    {
                        selectedMenuItem--;
                        if (selectedMenuItem < 0)
                            selectedMenuItem = menuItems.Count - 1;
                    } while (menuItems[selectedMenuItem].IsLabel);
                    break;
                case ConsoleKey.Enter:
                    menuItems[selectedMenuItem].OnClick();
                    break;
            }
            MenuManager.ShowActiveMenu();
        }
    }
}
