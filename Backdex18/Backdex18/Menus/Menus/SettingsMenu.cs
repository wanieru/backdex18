﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backdex18
{
    class SettingsMenu : Menu
    {
        public SettingsMenu(Menu back) : base("Settings", back)
        {
        }
        protected override void UpdateItems()
        {
            Random random = new Random();
            menuItems.Clear();
            for (var i = 0; i < 5; i++)
            {
                menuItems.Add(new MenuItem(random.Next(0, 100).ToString(), () => { }));
            }
            base.UpdateItems();
        }
    }
}
